package com.example.projetomariareal;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;


public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    String email, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void SignIn(View view) {
        EditText email = (EditText) findViewById(R.id.editTextTextEmailAddress);
        final String email_string = email.getText().toString();
        EditText password = (EditText) findViewById(R.id.editTextTextPassword);
        final String password_string = password.getText().toString();
        this.email = email_string;
        this.password = password_string;
        if (this.email.equals("") || this.password.equals("")){
            TextView t = (TextView) findViewById(R.id.errorSignInTextView);
            t.setText("Preencha todos os campos");
            return;
        }

        System.out.println("Seu email é " + email_string +" e sua senha é " + password_string);
        mAuth.signInWithEmailAndPassword(this.email, this.password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    TextView t = (TextView) findViewById(R.id.errorSignInTextView);
                    t.setText("Aí sim amigo!");
                    FirebaseUser user = mAuth.getCurrentUser();
                    Intent feedIntent = new Intent(MainActivity.this, Feed.class);
                    feedIntent.putExtra("USER_EMAIL", email_string);
                    feedIntent.putExtra("USER_PASSWORD", password_string);

                    startActivity(feedIntent);
                } else {
                    TextView t = (TextView) findViewById(R.id.errorSignInTextView);
                    t.setText("Poxa amigo, falhou ein!");
                    Log.w("DEBUG", "signInWithEmail:failure", task.getException());
                    Toast.makeText(MainActivity.this, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
//                    updateUI(null);
                }

                // ...
            }
        });
        this.mAuth.signInWithEmailAndPassword(email_string, password_string);

    }

    public void SignUp(View view) {
        startActivity(new Intent(MainActivity.this, Register.class));
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();



    }




}