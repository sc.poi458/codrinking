package com.example.projetomariareal;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

public class DeliveryScreen extends AppCompatActivity {
    String time, date, address_go, list, add_go, add_back, address_back;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void SendDelivery(View view) {
        EditText time = (EditText) findViewById(R.id.InputTimeBuy);
        final String time_string = time.getText().toString();
        EditText date = (EditText) findViewById(R.id.InputDateBuy);
        final String date_string = date.getText().toString();
        EditText address_go = (EditText) findViewById(R.id.InputAddressGoBuy);
        final String address_go_string = address_go.getText().toString();
        EditText add_go = (EditText) findViewById(R.id.InputAddressAddBuy);
        final String add_go_string = add_go.getText().toString();
        EditText address_back = (EditText) findViewById(R.id.InputAddressBackBuy);
        final String address_back_string = address_back.getText().toString();
        EditText add_back = (EditText) findViewById(R.id.InputAddressAddBackBuy);
        final String add_back_string = add_back.getText().toString();

        this.address_go = address_go_string;
        this.date = date_string;
        this.time = time_string;
        this.add_go = add_go_string;
        this.add_back = add_back_string;
        this.address_back = address_back_string;

        if (this.add_go.equals("")|| this.time.equals("")|| this.address_go.equals("")|| this.date.equals("")){
            TextView t = (TextView) findViewById(R.id.errorSignInTextView);
            t.setText("Preencha todos os campos");
            return;
        }

        DatabaseReference user = database.getReference("deliver/");
        DatabaseReference buy_user = user.push();
        buy_user.setValue(
                Arrays.asList(time_string, date_string,  address_go_string, add_go_string,  address_back_string, add_back_string)
        );
    }
}