package com.example.projetomariareal;

import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;


public class Register extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private final String TAG = "DEBUG";
    private String email, password, name, phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



    }

    public void SignUp(View view) {
        EditText email = (EditText) findViewById(R.id.editTextTextEmailRegister);
        final String email_string = email.getText().toString();
        EditText password = (EditText) findViewById(R.id.editTextPasswordRegister);
        final String password_string = password.getText().toString();
        EditText name = (EditText) findViewById(R.id.editTextUsername);
        final String name_string = name.getText().toString();
        EditText phone = (EditText) findViewById(R.id.editTextPhone);
        final String phone_string = phone.getText().toString();
        this.name = name_string;
        this.email = email_string;
        this.password = password_string;
        this.phone = phone_string;

        mAuth.createUserWithEmailAndPassword(this.email, this.password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    final FirebaseUser user = mAuth.getCurrentUser();
                    if (user != null) {
                        user.getIdToken(true)
                                .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                                        if (task.isSuccessful()) {
//                                            String idToken = task.getResult().getToken();
                                            String idToken = user.getUid();
                                            DatabaseReference user = database.getReference("users/"+idToken);
                                            user.setValue(
                                                    Arrays.asList(name_string, email_string,  phone_string)
                                            );



                                            // Send token to your backend via HTTPS
                                            // ...
                                        } else {
                                            // Handle error -> task.getException();
                                        }
                                    }
                                });
                        finish();
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                    Toast.makeText(Register.this, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
        this.mAuth.createUserWithEmailAndPassword(email_string, password_string);

    }
}