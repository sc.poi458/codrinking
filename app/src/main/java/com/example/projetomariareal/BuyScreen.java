package com.example.projetomariareal;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

public class BuyScreen extends AppCompatActivity {
    String time, date, address, list, add;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void SendBuy(View view) {
        EditText time = (EditText) findViewById(R.id.InputTimeBuy);
        final String time_string = time.getText().toString();
        EditText date = (EditText) findViewById(R.id.InputDateBuy);
        final String date_string = date.getText().toString();
        EditText list = (EditText) findViewById(R.id.InputListBuy);
        final String list_string = list.getText().toString();
        EditText address = (EditText) findViewById(R.id.InputAddressBuy);
        final String address_string = address.getText().toString();
        EditText add = (EditText) findViewById(R.id.InputAddressAddBuy);
        final String add_string = add.getText().toString();
        this.address = address_string;
        this.date = date_string;
        this.time = time_string;
        this.add = add_string;
        this.list = list_string;
        if (this.list.equals("") || this.add.equals("")|| this.time.equals("")|| this.address.equals("")|| this.date.equals("")){
            TextView t = (TextView) findViewById(R.id.errorSignInTextView);
            t.setText("Preencha todos os campos");
            return;
        }
        DatabaseReference user = database.getReference("buy/");
        DatabaseReference buy_user = user.push();
        buy_user.setValue(
                Arrays.asList(time_string, date_string,  address_string, add_string, list_string)
        );
    }
}