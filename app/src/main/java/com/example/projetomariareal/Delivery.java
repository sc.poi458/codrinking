package com.example.projetomariareal;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Delivery extends AppCompatActivity {
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private FirebaseDatabase database;
    private Map<String, Object> PostMap;
    private RecyclerView recyclerView;
    private final String TAG = "DEBUG";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        database = FirebaseDatabase.getInstance();

        DatabaseReference posts = database.getReference("buy/");

        posts.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
//                GenericTypeIndicator<Object> genericTypeIndicator = new GenericTypeIndicator<Object>() {};
//                Object PostMap = dataSnapshot.getValue(genericTypeIndicator );
//                List<String> final_posts =  new ArrayList<String>();
                GenericTypeIndicator<Map<String, ArrayList<String>>> genericTypeIndicator = new GenericTypeIndicator<Map<String, ArrayList<String>>>() {
                };
                Map<String, ArrayList<String>> PostMap = dataSnapshot.getValue(genericTypeIndicator);
                List<String> final_posts = new ArrayList<String>();
                if (PostMap != null) {
                    Log.d(TAG, "Value is: " + PostMap.toString());
                    for (Map.Entry<String, ArrayList<String>> id : PostMap.entrySet()) {


                        final_posts.add(id.getValue().get(0) + "  " + id.getValue().get(1) + "  " + id.getValue().get(2) + "  " + id.getValue().get(3) + "  " + id.getValue().get(4) );

                    }


                    Log.d("DEBUG", PostMap.toString());
                    MyAdapter mAdapter = new MyAdapter(final_posts);
                    recyclerView.setAdapter(mAdapter);
                }
            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

    }

    public void goToDelivery(View view){
        startActivity(new Intent(Delivery.this, DeliveryScreen.class));
    }

    public void goToBuy(View view){
        startActivity(new Intent(Delivery.this, BuyScreen.class));
    }



}