package com.example.projetomariareal;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;


public class Feed extends AppCompatActivity {
    private FirebaseUser user;
    private FirebaseAuth mAuth;
    private final String TAG = "DEBUG";
    private FirebaseDatabase database;
    private Map<String, Object> PostMap;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final String email_string = getIntent().getStringExtra("USER_EMAIL");
        final String password_string = getIntent().getStringExtra("USER_PASSWORD");

        mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithEmailAndPassword(email_string, password_string).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    user = mAuth.getCurrentUser();
                } else {

                    Log.w("DEBUG", "signInWithEmail:failure", task.getException());

                }

                // ...
            }
        });
        this.mAuth.signInWithEmailAndPassword(email_string, password_string);
        
        FloatingActionButton fab = findViewById(R.id.fab_feed);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Feed.this, Delivery.class));
                Log.d(TAG, "Pei pei");
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        database = FirebaseDatabase.getInstance();


        DatabaseReference posts = database.getReference("posts/");

        posts.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                GenericTypeIndicator<Map<String, HashMap<String,String>>> genericTypeIndicator = new GenericTypeIndicator<Map<String, HashMap<String,String>>>() {};
                Map<String, HashMap<String, String>> PostMap = dataSnapshot.getValue(genericTypeIndicator );
                List<String> final_posts =  new ArrayList<String>();
                if (PostMap != null) {
                    Log.d(TAG, "Value is: " + PostMap.toString());
                    for (Map.Entry<String, HashMap<String,String>> id : PostMap.entrySet()) {
                        Log.d(TAG, id.getKey());
                        for (Map.Entry<String, String>  user_post : id.getValue().entrySet()) {
                            Log.d(TAG, user_post.getKey());
                            Log.d(TAG, user_post.getValue());
                            final_posts.add(user_post.getValue());

                        }

                    }
                }
                MyAdapter mAdapter = new MyAdapter(final_posts);
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        // specify an adapter (see also next example)




    }



    public void MakePost(View view){
        EditText editText = (EditText) findViewById(R.id.MultiLine);
        final String text = String.valueOf(editText.getText());
        if (text == null) return;
        if (user == null) return;

        final String[] username = new String[1];
        DatabaseReference users = database.getReference("users/");
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<Map<String, ArrayList<String>>> genericTypeIndicator = new GenericTypeIndicator<Map<String, ArrayList<String>>>() {};
                Map<String, ArrayList<String>> PostMap = dataSnapshot.getValue(genericTypeIndicator );
//                GenericTypeIndicator<Map<String, Object>> genericTypeIndicator = new GenericTypeIndicator<Map<String, Object>>() {};
//                Map<String, Object> PostMap = dataSnapshot.getValue(genericTypeIndicator );

                String post = PostMap.get(user.getUid()).get(0) + ": " + text;
                DatabaseReference posts_from_user = database.getReference("posts/"+user.getUid()).push();
                posts_from_user.setValue(post);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // ...
            }
        });




    }

    public void goToShopping(View view){
        startActivity(new Intent(Feed.this, Delivery.class));

        Log.d(TAG, "Pei pei");
    }
}